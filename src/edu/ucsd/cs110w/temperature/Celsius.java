package edu.ucsd.cs110w.temperature;

public class Celsius extends Temperature
{
  public Celsius(float t)
  {
	  super(t);
  }
  public String toString()
  {
	  return getValue() + " C";
  }
@Override
public Temperature toCelsius() {
	// TODO Auto-generated method stub
	
	return this;
}
@Override
public Temperature toFahrenheit() {
	// TODO Auto-generated method stub
	float value = getValue();
	value = value * 9/5 + 32;
	Temperature Fah = new Fahrenheit(value);
	return Fah;
}
public Temperature toKelvin(){
	return null;
}
}
