package edu.ucsd.cs110w.temperature;

public class Fahrenheit extends Temperature
{
  public Fahrenheit(float t)
  {
	  super(t);
  }
  public String toString()
  {
	  return getValue() + " F";
  }
@Override
public Temperature toCelsius() {
	// TODO Auto-generated method stub
	float value = getValue();
	value = (value - 32) * 5/9;
	Temperature Cels = new Celsius(value);
	return Cels;
}
@Override
public Temperature toFahrenheit() {
	// TODO Auto-generated method stub
	return this;
}
public Temperature toKelvin(){
	return null;
}
}
